package time;

import static org.junit.Assert.*;

// Jawad Khan | 991556484

import org.junit.Test;

public class TimeTest {
	
	// Test getTotalMilliseconds
	
	@Test
	public void testGetTotalMillisecondsRegular() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:05");
		assertTrue("", totalMilliseconds == 05);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalMillisecondsExceptional() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:0A");
		fail("Invalid number of milliseconds");
	}
	
	@Test
	public void testGetTotalMillisecondsBoundaryIn() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:999");
		assertTrue("", totalMilliseconds == 999);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalMillisecondsBoundaryOut() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:1000");
		fail("Invalid number of milliseconds");
	}

	// Test getTotalSeconds
	
	@Test
	public void testGetTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds( "01:01:01" );
		assertTrue( "The time provided does not match the result", totalSeconds == 3661 );
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsException() {
		int totalSeconds = Time.getTotalSeconds( "01:O1:TA" );
		fail( "The provided time is not valid." );
	}
	
	@Test
	public void testGetTotalSecondsBoundaryIn() {
		int totalSeconds = Time.getTotalSeconds( "00:00:01" );
		assertTrue( "The time provided does not match the result", totalSeconds == 1 );
	}

	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsBoundaryOut() {
		int totalSeconds = Time.getTotalSeconds( "01:00:60" );
		fail( "The provided time is not valid" );
	}
	
}
